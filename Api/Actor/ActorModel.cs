﻿using Api.MovieActor;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Actor {
	public class ActorModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Birthplace { get; set; }
		public List<MovieActorMovieModel> Movies { get; set; }
	}
}
