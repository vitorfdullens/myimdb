﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Actor {
	public class ActorInMovieModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Birthplace { get; set; }
	}
}
