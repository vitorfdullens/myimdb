﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Actor {
	public class ActorData {
		[Required(ErrorMessage = "The name of the actor is required")]
		[MaxLength(100, ErrorMessage ="The name can't be greater than {1}")]
		public string Name { get; set; }
		[Required(ErrorMessage = "The actor birthplace is required")]
		[MaxLength(100, ErrorMessage = "The actor birthplace can't be greater than {1}")]
		public string Birthplace { get; set; }
	}
}
