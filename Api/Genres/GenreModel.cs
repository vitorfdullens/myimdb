﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Genres {
	public class GenreModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}
