﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Movies {
	public class MovieData {
		[Required(ErrorMessage = "Movie rank is required")]
		public int Rank { get; set; }
		[MaxLength(100, ErrorMessage = "The title can't be greater than {1}")]
		[Required(ErrorMessage = "Movie title is required")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Movie year is required")]
		[Range(1800, 2020, ErrorMessage = "Year should be between {1} and {2}")]
		public int Year { get; set; }
		[MaxLength(200, ErrorMessage = "The storyline can't be greater than {1}")]
		public string Storyline { get; set; }
		[Required(ErrorMessage = "Movie genre is required")]
		public Guid GenreId { get; set; }
	}
}
