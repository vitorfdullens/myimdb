﻿using Api.Genres;
using Api.MovieActor;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Movies {
	public class MovieModel {
		public Guid Id { get; set; }
		public int Rank { get; set; }
		public string Title { get; set; }
		public int Year { get; set; }
		public string Storyline { get; set; }
		public GenreModel Genre { get; set; }
		public List<MovieActorActorModel> Actors { get; set; }
	}
}
