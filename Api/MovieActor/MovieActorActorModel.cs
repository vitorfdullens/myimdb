﻿using Api.Actor;
using Api.Movies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.MovieActor {
	public class MovieActorActorModel {
		public ActorInMovieModel Actor { get; set; }
		public string Character { get; set; }
	}
}
