﻿using Api.Movies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.MovieActor {
	public class MovieActorMovieModel {
		public MovieInActorModel Movie { get; set; }
		public string Character { get; set; }
	}
}
