﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.MovieActor {

	public class MovieActorData {
		[MaxLength(100, ErrorMessage = "The character name can't be greater than {1}")]
		[Required(ErrorMessage = "Character is required")]
		public string Character { get; set; }
		[Required(ErrorMessage = "Actor is required")]
		public Guid ActorId { get; set; }
		[Required(ErrorMessage = "Movie is required")]
		public Guid MovieId { get; set; }

	}
}
