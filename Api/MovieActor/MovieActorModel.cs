﻿using Api.Actor;
using Api.Movies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.MovieActor {
	public class MovieActorModel {
		public Guid Id { get; set; }
		public MovieModel Movie { get; set; }
		public ActorModel Actor { get; set; }
		public string Character { get; set; }
	}
}
