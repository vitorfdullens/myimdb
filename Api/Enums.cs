﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api
{
    public class Enums {
		public enum ErrorCodes {
			Unknown = 0,
			MovieTitleAlreadyExists,
			MovieNotFound,
			GenreNotFound,
			GenreNameAlreadyExists,
			ActorNotFound,
			MovieActorNotFound
		}
    }
}
