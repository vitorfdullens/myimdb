using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Configuration;
using MyImdb.Entities;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using System.Threading.Tasks;

namespace MyImdb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

			services.AddControllersWithViews(o => {
				o.Filters.Add<ValidateModelStateAttribute>();
				o.Filters.Add<HandleExceptionFilter>();
			}).AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore).AddRazorRuntimeCompilation();

			services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
			services.AddScoped<MovieRepository>();
			services.AddScoped<MovieService>();
			services.AddScoped<ExceptionBuilder>();
			services.AddScoped<GenreRepository>();
			services.AddScoped<GenreService>();
			services.AddScoped<MovieActorRepository>();
			services.AddScoped<MovieActorService>();
			services.AddScoped<ActorRepository>();
			services.AddScoped<ActorService>();
			services.AddScoped<ModelConverter>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
				//endpoints.mapcontrollerroute(
				//    name: "default",
				//    pattern: "{controller=home}/{action=index}/{id?}");
				endpoints.MapControllers();
            });
        }
    }
}
