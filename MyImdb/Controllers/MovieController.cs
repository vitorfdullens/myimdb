﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Movies;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Models;
using MyImdb.Services;
using MyImdb.ViewModels;

namespace MyImdb.Controllers
{

	[ApiController]
	[Route("api/movies")]
	public class MovieController {

		private readonly MovieRepository movieRepository;
		private readonly ModelConverter mc;
		private readonly MovieService movieService;

		public MovieController(
			MovieRepository movieRepository,
			ModelConverter mc,
			MovieService movieService
		) {
			this.movieRepository = movieRepository;
			this.mc = mc;
			this.movieService = movieService;
		}

		[HttpGet] 
		public async Task<List<MovieModel>> List(int n = 20) { 
			var movies = await movieRepository.SelectTopNAsync(n); 
			return movies.ConvertAll(g => mc.ToModel(g)); 
		}

		[HttpGet("{id}")]
		public async Task<MovieModel> Get(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);

			return mc.ToModel(movie);
		}

		[HttpPost]
		public async Task<MovieModel> Create(MovieData request) {
			var movie = await movieService.CreateAsync(
				rank: request.Rank,
				title: request.Title,
				year: request.Year,
				storyline: request.Storyline,
				genreId: request.GenreId
				);

			return mc.ToModel(movie);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);

			await movieService.DeleteAsync(movie);
		}

		[HttpPut("{id}")]
		public async Task<MovieModel> Update(Guid id, MovieData request) {
			var movie = await movieRepository.SelectByIdAsync(id);

			await movieService.UpdateAsync(movie, request.Rank, request.Title, request.Year, request.Storyline, request.GenreId);

			return mc.ToModel(movie);
		}

	}
}
