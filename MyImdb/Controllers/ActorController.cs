﻿using Api.Actor;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/actors")]
	public class ActorController {
		private readonly ActorRepository actorRepository;
		private readonly ActorService actorService;
		private readonly ModelConverter mc;

		public ActorController(
			ActorRepository actorRepository,
			ActorService actorService,
			ModelConverter mc
			) {
			this.actorRepository = actorRepository;
			this.actorService = actorService;
			this.mc = mc;
		}

		[HttpPost]
		public async Task<ActorModel> Create(ActorData request) {
			var actor = await actorService.CreateAsync(request.Name, request.Birthplace);
			return mc.ToModel(actor);
		}


		[HttpGet]
		public async Task<List<ActorModel>> List(int n = 20) {
			var actors = await actorRepository.SelectTopNAsync(n);

			return actors.ConvertAll(a => mc.ToModel(a));
		}

		[HttpGet("{id}")]
		public async Task<ActorModel> Get(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);

			return mc.ToModel(actor);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);

			await actorService.DeleteAsync(actor);
		}

		[HttpPut("{id}")]
		public async Task<ActorModel> Update(Guid id, ActorData request) {
			var actor = await actorRepository.SelectByIdAsync(id);

			await actorService.UpdateAsync(actor, request.Name, request.Birthplace);

			return mc.ToModel(actor);
		}

	}
}
