﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	[ApiController]
	public class SystemController {
		[HttpGet("/api/system")]
		public object GetSystemInfo() {
			var assembly = Assembly.GetEntryAssembly();
			var assemblyName = assembly.GetName();
			var version = assemblyName.Version;

			return new {
				version = $"{version.Major}.{version.Minor}.{version.Build}",
			};
		}
	}
}
