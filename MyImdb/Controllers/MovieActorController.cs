﻿using Api.MovieActor;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/movieactors")]
	public class MovieActorController {
		private readonly MovieActorRepository movieActorRepository;
		private readonly ModelConverter mc;
		private readonly MovieActorService movieActorService;

		public MovieActorController(
			MovieActorRepository movieActorRepository,
			ModelConverter mc,
			MovieActorService movieActorService
		) {
			this.movieActorRepository = movieActorRepository;
			this.mc = mc;
			this.movieActorService = movieActorService;
		}

		[HttpPost]
		public async Task<MovieActorModel> Create(MovieActorData request) {
			var movieActor = await movieActorService.CreateAsync(
				actorId: request.ActorId,
				movieId: request.MovieId,
				character: request.Character
				);

			return mc.ToModel(movieActor);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);

			await movieActorService.DeleteAsync(movieActor);
		}

		[HttpPut("{id}")]
		public async Task<MovieActorModel> Update(Guid id, MovieActorData request) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);

			await movieActorService.UpdateAsync(movieActor, request.ActorId, request.MovieId, request.Character);

			return mc.ToModel(movieActor);
		}

	}
}
