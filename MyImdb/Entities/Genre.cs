﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Entities {
	public class Genre {
		public Guid Id { get; set; }
		[MaxLength(100)]
		[Required]
		public string Name { get; set; }
		public List<Movie> Movies { get; set; }

	}
}
