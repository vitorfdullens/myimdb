﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Entities
{
    public class Movie
    {
        public Guid Id { get; set; }
		public int Rank { get; set; }
		[MaxLength(100)]
		[Required]
        public string Title { get; set; }
        public int Year { get; set; }
		[MaxLength(200)]
        public string Storyline { get; set; }
		public DateTime CreationDateUtc { get; set; }
		[NotMapped]
		public DateTimeOffset CreationDate { 
			get {
				return new DateTimeOffset(CreationDateUtc, TimeSpan.Zero);
			} 
			set {
				CreationDateUtc = value.UtcDateTime;
			}
		}
		public Guid GenreId { get; set; }
		public Genre Genre { get; set; }

		public List<MovieActor> MovieActors { get; set; }
    }
}
