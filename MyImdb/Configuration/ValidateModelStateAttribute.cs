﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MyImdb.Configuration {
	public class ValidateModelStateAttribute : ActionFilterAttribute {
		private static readonly DefaultContractResolver SharedContractResolver = new DefaultContractResolver {
			NamingStrategy = new CamelCaseNamingStrategy {
				ProcessDictionaryKeys = true,
			},
		};

		private static readonly JsonSerializerSettings SerializerSettings;

		static ValidateModelStateAttribute() {
			SerializerSettings = new JsonSerializerSettings();
			SerializerSettings.ContractResolver = SharedContractResolver;
		}

		public override void OnActionExecuting(ActionExecutingContext context) {
			if (!context.ModelState.IsValid) {
				context.Result = new JsonResult(new SerializableError(context.ModelState), SerializerSettings) { 
					StatusCode = (int)HttpStatusCode.BadRequest
				};
			}
		}
	}
}
