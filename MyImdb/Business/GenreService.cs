﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class GenreService {
		private readonly GenreRepository genreRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;
		private readonly MovieRepository movieRepository;

		public GenreService(
			GenreRepository genreRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext,
			MovieRepository movieRepository
			) {
			this.genreRepository = genreRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
			this.movieRepository = movieRepository;
		}

		public async Task<Genre> CreateAsync(string name) {
			var existingGenre = await genreRepository.SelectByNameAsync(name);
			if (existingGenre != null) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.GenreNameAlreadyExists, new { name } );
			}

			var genre = await genreRepository.CreateAsync(name);

			await dbContext.SaveChangesAsync();
			return genre;
		}

		public async Task UpdateAsync(Genre genre, string name) {
			if (await dbContext.Genres.AnyAsync(g=>g.Name == name && g.Id != genre.Id)) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.GenreNameAlreadyExists, new { name });
			}

			genre.Name = name;

			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Genre genre) {
			var movies = await movieRepository.SelectByGenreIdAsync(genre.Id);

			dbContext.RemoveRange(movies);
			dbContext.Remove(genre);

			await dbContext.SaveChangesAsync();
		}
	}
}
