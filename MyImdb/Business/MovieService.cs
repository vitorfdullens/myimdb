﻿using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api;
using Microsoft.EntityFrameworkCore;

namespace MyImdb.Business {
	public class MovieService {
		private readonly MovieRepository movieRepository; 
		private readonly ExceptionBuilder exceptionBuilder; 
		private readonly AppDbContext dbContext;
		private readonly GenreRepository genreRepository;
		public MovieService(
		    MovieRepository movieRepository, 
			ExceptionBuilder exceptionBuilder, 
			AppDbContext dbContext,
			GenreRepository genreRepository
		) { 
			this.movieRepository = movieRepository; 
			this.exceptionBuilder = exceptionBuilder; 
			this.dbContext = dbContext;
			this.genreRepository = genreRepository;
		}
		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Guid genreId) {
			var existingMovie = await movieRepository.SelectByTitleAsync(title);
			if (existingMovie != null) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			var genre = await genreRepository.SelectByIdAsync(genreId);

			var movie = await movieRepository.CreateAsync(rank, title, year, storyline, genre.Id); 

			await dbContext.SaveChangesAsync(); 
			return movie; 
		}

		public async Task DeleteAsync(Movie movie) {
			dbContext.Remove(movie);

			await dbContext.SaveChangesAsync();
		}

		public async Task UpdateAsync(Movie movie, int rank, string title, int year, string storyline, Guid genreId) {
			if (await dbContext.Movies.AnyAsync(m => m.Title == title && m.Id != movie.Id)) {
				throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			movie.Rank = rank;
			movie.Title = title;
			movie.Year = year;
			movie.Storyline = storyline;

			var genre = await genreRepository.SelectByIdAsync(genreId);

			movie.GenreId = genre.Id;

			await dbContext.SaveChangesAsync();
		}
	}
}
