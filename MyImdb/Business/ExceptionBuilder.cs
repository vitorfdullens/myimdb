﻿using Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Enums;

namespace MyImdb.Business {
	public class ExceptionBuilder{
		public ApiException Api(ErrorCodes code, object details = null) { 
			var message = string.Empty; 
			switch (code) {
				case ErrorCodes.Unknown:
					message = "An Unknown error has occurred"; 
					break;
				case ErrorCodes.MovieTitleAlreadyExists:
					message = "A movie with the provided title alreay exists";
					break;
				case ErrorCodes.GenreNotFound:
					message = "The genre was not found";
					break;
				case ErrorCodes.GenreNameAlreadyExists:
					message = "The genre with the provided name already exists";
					break;
				case ErrorCodes.MovieNotFound:
					message = "The movie was not found";
					break;
				case ErrorCodes.ActorNotFound:
					message = "The actor was not found";
					break;
				case ErrorCodes.MovieActorNotFound:
					message = "The relationship between Movie and Actors was not found";
					break;
				default:
					break; 
			}
			return new ApiException(new ErrorModel() { Code = code, Message = message, Details = getDetailsDictionary(details) });
		}
	private static Dictionary<string, string> getDetailsDictionary(object details) { 
			var dic = new Dictionary<string, string>(); 
			foreach (var descriptor in details.GetType().GetProperties()) { 
				dic[descriptor.Name] = descriptor.GetValue(details, null)?.ToString() ?? "null"; 
			} 
			return dic; 
		}
	}
}
