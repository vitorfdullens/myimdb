﻿using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class MovieActorService {
		private readonly MovieRepository movieRepository;
		private readonly ActorRepository actorRepository;
		private readonly AppDbContext dbContext;
		private readonly MovieActorRepository movieActorRepository;

		public MovieActorService(
			MovieRepository movieRepository,
			ActorRepository actorRepository,
			AppDbContext dbContext,
			MovieActorRepository movieActorRepository
			) {
			this.movieRepository = movieRepository;
			this.actorRepository = actorRepository;
			this.dbContext = dbContext;
			this.movieActorRepository = movieActorRepository;
		}

		public async Task<MovieActor> CreateAsync (Guid actorId, Guid movieId, string character) {
			var movie = await movieRepository.SelectByIdAsync(movieId);
			var actor = await actorRepository.SelectByIdAsync(actorId);

			var movieActor = await movieActorRepository.CreateAsync(actor.Id, movie.Id, character);

			await dbContext.SaveChangesAsync();
			return movieActor;
		}

		public async Task DeleteAsync(MovieActor movieActor) {
			dbContext.Remove(movieActor);

			await dbContext.SaveChangesAsync();
		}

		public async Task UpdateAsync(MovieActor movieActor, Guid actorId, Guid movieId, string character) {

			movieActor.Character = character;

			var movie = await movieRepository.SelectByIdAsync(movieId);
			var actor = await actorRepository.SelectByIdAsync(actorId);

			movieActor.ActorId = actor.Id;
			movieActor.MovieId = movie.Id;

			await dbContext.SaveChangesAsync();

		}

	}
}
