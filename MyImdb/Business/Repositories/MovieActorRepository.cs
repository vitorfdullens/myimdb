﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Repositories {
	public class MovieActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieActorRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<MovieActor> CreateAsync(Guid actorId, Guid movieId, string character) {
			var movieActor = new MovieActor() {
				Id = Guid.NewGuid(),
				Character = character,
				ActorId = actorId,
				MovieId = movieId
			};

			await dbContext.AddAsync(movieActor);
			return movieActor;
		}

		public async Task<MovieActor> SelectByIdAsync(Guid id) {
			return await dbContext.MovieActors.Include(ma => ma.Actor).Include(ma => ma.Movie).FirstOrDefaultAsync(ma => ma.Id == id)
				?? throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieNotFound, new { id });

		}
	}
}
