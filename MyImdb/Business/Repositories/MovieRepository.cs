﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Repositories {
	public class MovieRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}
		public async Task<List<Movie>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Movies.Include(m => m.Genre).Include(m => m.MovieActors).ThenInclude(ma => ma.Actor).OrderBy(m => m.Title).AsQueryable();
			query = query.Take(n);
			return await query.ToListAsync();
		}

		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Guid genreId) {

			var movie = new Movie() {
				Id = Guid.NewGuid(),
				GenreId = genreId,
				Rank = rank,
				Title = title,
				Year = year,
				Storyline = storyline,
				CreationDate = DateTimeOffset.Now
			}; 
			await dbContext.AddAsync(movie);
			return movie; 
		}

		public async Task<Movie> SelectByTitleAsync(string title) {
			return await dbContext.Movies.Include(m => m.Genre).Include(m => m.MovieActors).ThenInclude(ma => ma.Actor).FirstOrDefaultAsync(m => m.Title == title);
		}

		public async Task<List<Movie>> SelectByGenreIdAsync(Guid genreId) {
			var query = dbContext.Movies.Include(m => m.Genre).Include(m => m.MovieActors).ThenInclude(ma => ma.Actor).Where(m => m.GenreId == genreId);

			return await query.ToListAsync();
		}

		public async Task<Movie> SelectByIdAsync(Guid id) {
			return await dbContext.Movies.Include(m=> m.Genre).Include(m => m.MovieActors).ThenInclude(ma => ma.Actor).FirstOrDefaultAsync(m => m.Id == id)
				?? throw exceptionBuilder.Api(Api.Enums.ErrorCodes.MovieNotFound, new { id });

		}


	}
}
