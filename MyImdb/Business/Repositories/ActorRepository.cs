﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Repositories {
	public class ActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public ActorRepository(
			AppDbContext dbContext,
			ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<List<Actor>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Actors.Include(a => a.MovieActors).ThenInclude(ma => ma.Movie).ThenInclude(m => m.Genre).OrderBy(a => a.Name).AsQueryable();
			query = query.Take(n);
			return await query.ToListAsync();
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var actor = new Actor() {
				Id = Guid.NewGuid(),
				Name = name,
				Birthplace = birthplace
			};

			await dbContext.AddAsync(actor);
			return actor;
		}

		public async Task<Actor> SelectByIdAsync(Guid id) {
			return await dbContext.Actors.Include(a => a.MovieActors).ThenInclude(ma => ma.Movie).ThenInclude(m => m.Genre).FirstOrDefaultAsync(a => a.Id == id)
				?? throw exceptionBuilder.Api(Api.Enums.ErrorCodes.ActorNotFound, new { id });
		}


	}
}
