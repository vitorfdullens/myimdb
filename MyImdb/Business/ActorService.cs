﻿using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class ActorService {
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;
		private readonly ActorRepository actorRepository;

		public ActorService (
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext,
			ActorRepository actorRepository
			) {
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
			this.actorRepository = actorRepository;
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var actor = await actorRepository.CreateAsync(name, birthplace);

			await dbContext.SaveChangesAsync();
			return actor;
		}

		public async Task DeleteAsync(Actor actor) {
			dbContext.Remove(actor);

			await dbContext.SaveChangesAsync();
		}

		public async Task UpdateAsync(Actor actor, string name, string birthplace) {
			actor.Name = name;
			actor.Birthplace = birthplace;

			await dbContext.SaveChangesAsync();

		}

	}
}
