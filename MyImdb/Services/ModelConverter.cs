﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Actor;
using Api.Genres;
using Api.MovieActor;
using Api.Movies;
using MyImdb.Entities;

namespace MyImdb.Services {
	public class ModelConverter {
		public ModelConverter() { }
		public GenreModel ToModel(Genre genre) {
			return new GenreModel() {
				Id = genre.Id,
				Name = genre.Name
			};
		}
		public MovieInActorModel ToModelInActor(Movie movie) {
			return new MovieInActorModel() {
				Id = movie.Id,
				Title = movie.Title,
				Rank = movie.Rank,
				Year = movie.Year,
				Storyline = movie.Storyline,
				Genre = ToModel(movie.Genre),
			};
		}

		public MovieActorMovieModel ToModelMovie(MovieActor movieActor) {
			return new MovieActorMovieModel() {
				Movie = ToModelInActor(movieActor.Movie),
				Character = movieActor.Character
			};
		}

		public ActorModel ToModel(Actor actor) {
			return new ActorModel() {
				Id = actor.Id,
				Name = actor.Name,
				Birthplace = actor.Birthplace,
				Movies = actor.MovieActors?.ConvertAll(a => ToModelMovie(a))
			};
		}
		public ActorInMovieModel ToModelInMovie(Actor actor) {
			return new ActorInMovieModel() {
				Id = actor.Id,
				Name = actor.Name,
				Birthplace = actor.Birthplace,
			};
		}
		public MovieActorActorModel ToModelActor(MovieActor movieActor) {
			return new MovieActorActorModel() {
				Actor = ToModelInMovie(movieActor.Actor),
				Character = movieActor.Character
			};
		}

		public MovieModel ToModel(Movie movie) {
			return new MovieModel() {
				Id = movie.Id,
				Title = movie.Title,
				Rank = movie.Rank,
				Year = movie.Year,
				Storyline = movie.Storyline,
				Genre = ToModel(movie.Genre),
				Actors = movie.MovieActors?.ConvertAll(m => ToModelActor(m))
			}; 
		}

		public MovieActorModel ToModel(MovieActor movieActor) {
			return new MovieActorModel() {
				Id = movieActor.Id,
				Movie = ToModel(movieActor.Movie),
				Actor = ToModel(movieActor.Actor),
				Character = movieActor.Character
			};
		}
	}
}
